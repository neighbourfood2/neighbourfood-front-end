/*global angular */

/**
 * The main TodoMVC app module
 *
 * @type {angular.Module}
 */

$ = require("jquery");
jQuery = $;
require("bootstrap-sass")

angular = require('angular');
require('angular-route');


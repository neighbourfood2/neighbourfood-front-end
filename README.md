NeighbourFood Front-End
=======================

## Synopsis

## Features List

## Development

### Installation instructions

First make sure Gulp is globally installed, by running:

    npm install -g gulp

After cloning the project, run the following commands:

    npm install
    gulp
    gulp dev
    
This will start the development server, the app should be available at the following url:

[http://localhost:8000/dist/index.html](http://localhost:8000/dist/index.html)

### Contact

* NeighbourFood website: [neighbourfood.mx](http://www.neighbourfood.mx/contact)
* Lead Architect: [saul.ortigoza@neighbourfood.mx](mailto:saul.ortigoza@neighbourfood.mx)
* NeighbourFood Contact: [contacto@neighbourfood.mx](mailto:contacto@neighbourfood.mx)

### Documentation

* ./docs
* [Sharepoint](https://drive.google.com/drive/folders/0B47wJF8CvsssOGNwTy1IUlJsRWc)
* [Repositorio](https://bitbucket.org/neighbourfood2/neighborfood-server-ar)

### Testing

* Jasmine/PhantomJS/Karma
* [Bug incidents](https://bitbucket.org/neighbourfood2/neighbourfood-front-end/issues)

### Project Management

* [Scrumban board](https://trello.com/b/bviggIVA/neighbourfood-app)

### Servers

* Test server: [test.neighbourfood.mx](http://test.neighbourfood.mx/)
* Production server: [neighbourfood.mx](http://neighbourfood.mx/)

### Deployment

TBD

## CHANGELOG

### Work To Do

TBD

### HEAD


## License

Please see the {file:LICENSE} and {file:LEGAL} documents for more information.


# References

* [angularjs-gulp-example in Gitgub](https://github.com/jhades/angularjs-gulp-example)
* [angularjs-gulp-example blog post](http://blog.jhades.org/what-every-angular-project-likely-needs-and-a-gulp-build-to-provide-it/)


